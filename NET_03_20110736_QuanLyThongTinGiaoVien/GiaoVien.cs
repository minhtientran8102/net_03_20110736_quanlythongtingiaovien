﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_03_20110736_QuanLyThongTinGiaoVien
{
    public class GiaoVien : NguoiLaoDong
    {
        //Khởi tạo thêm thuộc tính hệ số lương
        private float heSoLuong;

        //Tạo Setter và Getter cho thuộc tính hệ số lương
        public float HeSoLuong { get => heSoLuong; set => heSoLuong = value; }

        //Hàm khởi tạo đối tượng khi chưa có thông tin
        public GiaoVien() : base() 
        {
            this.heSoLuong = 1f;
        }

        //Hàm khởi tạo đối tượng khi đã có thông tin
        public GiaoVien(string hoten, int namSinh, int luongCoBan, float heSoLuong) : base(hoten,namSinh,luongCoBan)
        {
            this.heSoLuong = heSoLuong;
        }

        //Hàm nhập thông tin
        public void NhapThongTin(float heSoLuong)
        {
            this.heSoLuong = heSoLuong;
        }

        //Hàm tính lương dành cho đối tượng là giáo viên
        public override int TinhLuong()
        {
            float value = (float)this.LuongCoBan * 1.25f * this.heSoLuong;
            return (int)value;
        }

        //Hàm xuất thông tin giáo viên
        public override void XuatThongtin()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine($"Ho ten la: {this.HoTen}, " +
                $"nam sinh: {this.NamSinh}, " +
                $"luong co ban: {this.LuongCoBan}, " +
                $"he so luong {this.heSoLuong}, " +
                $"luong {TinhLuong()}.");
        }

        //Hàm xử lý hệ số lương: Hệ sô lương + 0.6
        public void XuLy()
        {
            this.heSoLuong = this.heSoLuong + 0.6f;
        }
       // public void CheckThongTin()
    }
}
