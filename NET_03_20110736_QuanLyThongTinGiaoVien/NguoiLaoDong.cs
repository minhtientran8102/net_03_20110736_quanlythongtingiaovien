﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_03_20110736_QuanLyThongTinGiaoVien
{
    public class NguoiLaoDong
    {
        //Thuộc tính chứa họ và tên người lao động
        private string hoTen;
        //Thuộc tính chứa năm sinh người lao động
        private int namSinh;
        //Thuộc tính chứa lương cơ bản người lao động
        private int luongCoBan;

        //Khởi tạo Setter và Getter
        public string HoTen { get => hoTen; set => hoTen = value; }
        public int NamSinh { get => namSinh; set => namSinh = value; }
        public int LuongCoBan { get => luongCoBan; set => luongCoBan = value; }

        //Tạo hàm khởi tạo đối tượng khi chưa có thông tin
        public NguoiLaoDong()
        {
            this.HoTen = null;
            this.NamSinh = DateTime.Now.Year;
            this.LuongCoBan = 0;
        }

        //Tạo hàm khởi tạo đối tượng khi đã có thông tin
        public NguoiLaoDong(string hoten, int namSinh, int luongCoBan)
        { 
                this.hoTen = hoten.Trim();
                this.namSinh = namSinh;
                this.luongCoBan = luongCoBan;              
        }

        //Tạo hàm nhập thông tin đối tượng khi chưa có thông tin
        public void NhapThongTin(string hoten, int namSinh, int luongCoBan)
        {
            this.hoTen = hoten.Trim();
            this.namSinh = namSinh;
            this.luongCoBan = luongCoBan;
        }

        //Tạo hàm trả về lương cơ bản
        public virtual int TinhLuong()
        {
            return this.luongCoBan;
        }

        //Tạo hàm xuất thông tin người lao động
        public virtual void XuatThongtin()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine($"Ho ten la: {hoTen}, nam sinh: {namSinh}, luong co ban: {luongCoBan}.");
        }
    }
}
