﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NET_03_20110736_QuanLyThongTinGiaoVien
{
    public class Program
    {
        //Khai báo list giáo viên
        public static List<GiaoVien> lstGiaoVien = new List<GiaoVien>();

        //Tạo hàm nhập danh sách giáo viên
        public static void NhapThongTinGiaoVien(List<GiaoVien> lstGiaoVien)
        {
            try
            {                
                int soLuong;
                Console.Write("MỜI NHẬP SỐ LƯỢNG GIÁO VIÊN: ");
                soLuong = int.Parse(Console.ReadLine());
                for (int i = 0; i < soLuong; i++)
                {
                    bool check = false;
                    while (!check && i<soLuong)
                    {
                        Console.WriteLine($"Nhập thông tin giáo viên số 0{i + 1}: ");
                        Console.Write($"Họ và tên: ");
                        string hoTen = Console.ReadLine();
                        Console.Write($"Năm sinh: ");
                        int namSinh = int.Parse(Console.ReadLine());
                        Console.Write($"Lương cơ bản: ");
                        int luongCoBan = int.Parse(Console.ReadLine());
                        Console.Write($"Hệ số lương: ");
                        float heSoLuong = float.Parse(Console.ReadLine());
                        if(CheckThongTin(hoTen, namSinh, luongCoBan, heSoLuong))
                        { 
                            lstGiaoVien.Add(new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong));                            
                            check = true;
                        } 
                    }
                    Console.WriteLine("===============================");
                }
                Console.WriteLine("NHẬP DANH SÁCH GIÁO VIÊN THÀNH CÔNG!!!");
            }catch(Exception ex)
            {
                Console.WriteLine($"Đã có lỗi xảy ra: {ex.Message}");
                // Tìm và xử lý các lỗi cụ thể
                if (ex is ArgumentNullException)
                {
                    // Xử lý lỗi ArgumentNullException
                    Console.WriteLine("Lỗi: Tham số không được để trống.");
                    Console.WriteLine("Phương án sửa lỗi: Kiểm tra và cung cấp giá trị hợp lệ cho tham số.");
                }
                else if (ex is DivideByZeroException)
                {
                    // Xử lý lỗi DivideByZeroException
                    Console.WriteLine("Lỗi: Chia cho số 0.");
                    Console.WriteLine("Phương án sửa lỗi: Kiểm tra và chỉ định giá trị khác 0 để thực hiện phép chia.");
                }
                else
                {
                    // Xử lý các loại ngoại lệ khác
                    Console.WriteLine("Lỗi: Có lỗi xảy ra trong quá trình thực thi.");
                    Console.WriteLine("Phương án sửa lỗi: Kiểm tra và xử lý lỗi một cách cụ thể.");
                }
            }
        }

        //Xây dượng hàm tìm kiếm thông tin giáo viên có lương thấp nhất
        public static void GiaoVienLuongThapNhat(List<GiaoVien> lstGiaoVien)
        {
            try
            { 
                //Tìm giá trị mức lương thấp nhất
                var minSalary = lstGiaoVien.Min(gv => gv.TinhLuong());
                //Khai báo 1 list gồm những giáo viên có lương thấp nhất
                var lowestSalaryGiaoViens = lstGiaoVien.Where(gv => gv.TinhLuong() == minSalary).ToList();
                //In danh sách vừa tìm ra mà hình
                Console.WriteLine("Giáo viên có lương thấp nhất:");
                foreach (var i in lowestSalaryGiaoViens)
                {
                    i.XuatThongtin();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Đã có lỗi xảy ra: {ex.Message}");
            }

        }
        // Xây dựng hàm kiểm tra đầu vào của thông tin
        public static bool CheckThongTin(string hoTen, int namSinh, int luongCoBan, float heSoLuong)
        {
            bool check = false;
            //Kiểm tra họ và tên
            if(hoTen.Trim().Equals(""))
            {
                Console.WriteLine("Không được để trống họ và tên");
                return check;
            }
            //Kiểm tra năm sinh
            if (namSinh < 1900 || namSinh > DateTime.Now.Year)
            {
                Console.WriteLine("Năm sinh không hợp lệ!");
                return check; 
            }  
            //Kiểm tra mức lương cơ bản
            if (luongCoBan <= 0)
            {
                Console.WriteLine("Lương cơ bản không hợp lệ!");
                return check;
            }
            //Kiểm tra hệ số lương
            if (heSoLuong <= 0)
            {
                Console.WriteLine("Hệ số lương không hợp lệ!");
                return check;
            }
            check = true;
            return check;
        }
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //Gọi hàm để nhập thông tin giáo viên
            NhapThongTinGiaoVien(lstGiaoVien);
            //Gọi hàm tìm giáo viên có lương thấp nhất
            GiaoVienLuongThapNhat(lstGiaoVien);            
            
            Console.ReadLine();
        }
    }
}
